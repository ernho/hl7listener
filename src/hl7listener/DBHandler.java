package hl7listener;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Properties;

public class DBHandler {
	
	final String dbms = "mysql";
	final static String serverName = "localhost";
	final static String port = "3306";
	//final String dbURL = "jdbc:mysql://127.0.0.1:3306/";
	final static String dbName = "dev01";
	final String driver = "com.mysql.jdbc.Driver";
	public static String userName = "";
	public static String password = "";

	
	Connection conn = null;



public void  getDBConnection()  {

	InputStream inputStream = null;

	try {
		Properties connectionProps = new Properties();
		String propFileName = "database-configuration.properties";

		inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

		if (inputStream != null) {
			connectionProps.load(inputStream);
		} else {
		    connectionProps.put("user", userName);
		    connectionProps.put("password", password);
		}
	    Class.forName("com.mysql.jdbc.Driver");

	    this.conn = DriverManager.getConnection(
                "jdbc:mysql://" +
                serverName +
                ":" + port + "/"+dbName,
                connectionProps);
 
	    System.out.println("Connected to database");

	} catch (Exception e) {
		System.out.println("Exception: " + e);
	} 
	

    
   
}



public void close() {
	if(this.conn!=null)
		try {
			this.conn.close();
			System.out.println("Connection closed");
		} catch (SQLException e) {
			e.printStackTrace();
		}
}


public void checkPatient(int PatID, String surname, String givenname) {
    Statement statement;
    ResultSet rs;
	try {

		statement = this.conn.createStatement();
		String sql = "SELECT * FROM pat_tmp WHERE PatID =" +PatID+";";
	     rs = statement.executeQuery(sql);
	     System.out.println("Check for patient");
        if(!rs.next())
        {	System.out.println("New patient.....");
        	sql = "INSERT INTO pat_tmp(PatID,surname,givenname) VALUES ("+PatID+",'"+surname+"','"+givenname+"');";
        	statement.execute(sql);
        }
        
    	sql = "UPDATE pat_tmp  SET lastmsg=DEFAULT WHERE surname ='"+surname+"';";
    	statement.executeUpdate(sql);


	} catch (SQLException e) {
		e.printStackTrace();
	}    
}



public boolean saveMsg(int PatID, String msgType, HashMap<String, Integer> values, String txt,
		String imgName) {


    Statement statement;
	try {
		statement = this.conn.createStatement();
		String sql;
		System.out.println("Saving new message with type "+msgType);
	   
       
        	statement = this.conn.createStatement();

    		sql = "INSERT INTO rotem_messages(PatID,EncounterID,type,CT,A5,A10,A15,A20,A25,A30,"
    				+ "CFT,MCF,MCFt,alpha,LI30,LI45,LI60,ML,CFR,LOT,CLR,AR5,AR10,AR15,AR20,AR25,AR30"
    				+ ",MCE,ACF,G,TPI,MAXV,MAXVt,AUC,LT,image) VALUES"
    				+ "  ("+PatID+",42,'"+msgType+"'," + values.get("CT") + "," 
    	    				+ values.get("A5") + "," + values.get("A10") + ","+ values.get("A15") 
    	    				+ ","+ values.get("A20") + ","+ values.get("A25") + ","+ values.get("A30")
    	    				+ ","+ values.get("CFT") + ","+ values.get("MCF") + ","+ values.get("MCFt") 
    	    				+ ","+ values.get("alpha") + ","+ values.get("LI30")+ ","+ values.get("LI45")+","+ values.get("LI60")
    	    				+","+ values.get("ML")+","+ values.get("CFR")+","+ values.get("LOT")+","+ values.get("CLR")
    	    				+","+ values.get("AR5")+","+ values.get("AR10")+","+ values.get("AR15")+","+ values.get("AR20")+","+ values.get("AR25")+
    	    				","+ values.get("AR30")+
    	    				","+ values.get("MCE")+","+ values.get("ACF")+","+ values.get("G")+
    	    				","+ values.get("TPI")+","+ values.get("MAXV")+","+ values.get("MAXVt")
    	    				+","+ values.get("AUC")+","+ values.get("LT")+",'"+imgName+"');";
            statement.execute(sql);
            
       

        
        return true;
   

	} catch (SQLException e) {
		e.printStackTrace();
		return false;
	}  
	
	
	
}




}
