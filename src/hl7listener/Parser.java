package hl7listener;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;

import org.apache.commons.io.FileExistsException;
import org.apache.commons.io.FileUtils;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.PipeParser;
import ca.uhn.hl7v2.util.Terser;

public class Parser {

	String msgType;
	DBHandler dbHandler;
	private String path_to_copy_img;
	private String path_to_copy_msg;
	

	Parser() {

		this.dbHandler = new DBHandler();
	}
	
	
	

	protected void setPathToCopyMsg(String path_to_copy_msg) {
		this.path_to_copy_msg = path_to_copy_msg;
	}


	private String getPathToCopyMsg() {
		return path_to_copy_msg;
	}
	

	private String getPathToCopyImg() {
		return path_to_copy_img;
	}




	protected void setPathToCopyImg(String path_to_copy_img) {
		this.path_to_copy_img = path_to_copy_img;
	}


	static String readFile(String path, Charset encoding) {
		byte[] encoded = null;
		try {
			encoded = Files.readAllBytes(Paths.get(path, new String[0]));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new String(encoded, encoding);
	}

	void parseImg(String path, String imgName) {
		File source = null;
		File dest = null;

		try {
			source = new File(path + "/" + imgName);
			dest = new File(getPathToCopyImg() + "/" + imgName);
			FileUtils.moveFile(source, dest);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("Image copied to" + dest);
		dest.setExecutable(true, false);
		dest.setReadable(true, false);
		dest.setWritable(true, false);

	}
	/**
	 * Backup hl7 Message to path_to_copy_msg. In case the message already exists, 
	 * delete old message and try again.
	 * 
	 * @param filePath
	 * @param fileName
	 */
	void moveFile(String filePath, String fileName) {
		File source = null;
		File dest = null;

		source = new File(filePath + "/" + fileName);
		dest = new File(getPathToCopyMsg() + "/" + fileName);
		try {
			FileUtils.moveFile(source, dest);
		} catch (FileExistsException fe) {

			if (dest.delete()) {
				try {
					FileUtils.moveFile(source, dest);
				} catch (IOException e) {

				}

			}
		} catch (Exception e) {

		}

		System.out.println("Message copied to" + dest);
		source.delete();

	}

	/**
	 * 
	 * 
	 * 
	 * @param path
	 * @param fileName
	 */
	void parseMsg(String path, String fileName) {

		String msgContent = readFile(path + "/" + fileName, Charset.defaultCharset());
		msgContent = msgContent.replace('\n', '\r');
		PipeParser pipeParser = new PipeParser();

		try {

			Message message;
			message = pipeParser.parse(msgContent);
			Terser terser = new Terser(message);
			msgType = terser.get("/.OBR-15");
			System.out.println("INFO: New message received " + fileName + " type " + msgType);
			String surename = terser.get("/.PID-5");
			String givenname = terser.get("/.PID-5-2");

			String patID_s = terser.get("/.PID-2");
			int patID;
			if (patID_s.length() <= 8) {
				patID = Integer.parseInt(patID_s);

			} else {
				patID = Integer.parseInt(patID_s.substring(0, 8));

			}
			dbHandler.getDBConnection();
			dbHandler.checkPatient(patID, surename, givenname);

			extractValues(path, fileName, patID, message, terser);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void extractValues(String filePath, String fileName, int patID, Message message, Terser terser) {
		try {
			System.out.println("Message with ID " + terser.get("/.PID-2"));
			HashMap<String, Integer> values = new HashMap<String, Integer>();
			String imgName = terser.get("/.OBSERVATION(0)/.OBX-5");

			String msgType = terser.get("/.OBR-15");

			for (int i = 1; i <= 32; i++) {
				String record = terser.get("/.OBSERVATION(" + i + ")/.OBX-3-4");
				if (record != null) {

					switch (record) {
					case "CT":
						values.put("CT", Integer.parseInt(terser.get("/.OBSERVATION(" + i + ")/.OBX-5")));
						break;
					case "A5":
						values.put("A5", Integer.parseInt(terser.get("/.OBSERVATION(" + i + ")/.OBX-5")));
						break;
					case "A10":
						values.put("A10", Integer.parseInt(terser.get("/.OBSERVATION(" + i + ")/.OBX-5")));
						break;
					case "A15":
						values.put("A15", Integer.parseInt(terser.get("/.OBSERVATION(" + i + ")/.OBX-5")));
						break;
					case "A20":
						values.put("A20", Integer.parseInt(terser.get("/.OBSERVATION(" + i + ")/.OBX-5")));
						break;

					case "A25":
						values.put("A25", Integer.parseInt(terser.get("/.OBSERVATION(" + i + ")/.OBX-5")));
						break;
					case "A30":
						values.put("A30", Integer.parseInt(terser.get("/.OBSERVATION(" + i + ")/.OBX-5")));
						break;
					case "CFT":
						values.put("CFT", Integer.parseInt(terser.get("/.OBSERVATION(" + i + ")/.OBX-5")));
						break;
					case "MCF":
						values.put("MCF", Integer.parseInt(terser.get("/.OBSERVATION(" + i + ")/.OBX-5")));
						break;
					case "MCF-t":
						values.put("MCFt", Integer.parseInt(terser.get("/.OBSERVATION(" + i + ")/.OBX-5")));
						break;
					case "alpha":
						values.put("alpha", Integer.parseInt(terser.get("/.OBSERVATION(" + i + ")/.OBX-5")));
						break;
					case "LI30":
						values.put("LI30", Integer.parseInt(terser.get("/.OBSERVATION(" + i + ")/.OBX-5")));
						break;
					case "LI45":
						values.put("LI45", Integer.parseInt(terser.get("/.OBSERVATION(" + i + ")/.OBX-5")));
						break;
					case "LI60":
						values.put("LI60", Integer.parseInt(terser.get("/.OBSERVATION(" + i + ")/.OBX-5")));
						break;
					case "ML":
						values.put("ML", Integer.parseInt(terser.get("/.OBSERVATION(" + i + ")/.OBX-5")));
						break;
					case "CFR":
						values.put("CFR", Integer.parseInt(terser.get("/.OBSERVATION(" + i + ")/.OBX-5")));
						break;
					case "LOT":
						values.put("LOT", Integer.parseInt(terser.get("/.OBSERVATION(" + i + ")/.OBX-5")));
						break;
					case "CLR":
						values.put("CLR", Integer.parseInt(terser.get("/.OBSERVATION(" + i + ")/.OBX-5")));
						break;
					case "AR5":
						values.put("AR5", Integer.parseInt(terser.get("/.OBSERVATION(" + i + ")/.OBX-5")));
						break;
					case "AR10":
						values.put("AR10", Integer.parseInt(terser.get("/.OBSERVATION(" + i + ")/.OBX-5")));
						break;
					case "AR15":
						values.put("AR15", Integer.parseInt(terser.get("/.OBSERVATION(" + i + ")/.OBX-5")));
						break;
					case "AR20":
						values.put("AR20", Integer.parseInt(terser.get("/.OBSERVATION(" + i + ")/.OBX-5")));
						break;
					case "AR25":
						values.put("AR25", Integer.parseInt(terser.get("/.OBSERVATION(" + i + ")/.OBX-5")));
						break;
					case "AR30":
						values.put("AR30", Integer.parseInt(terser.get("/.OBSERVATION(" + i + ")/.OBX-5")));
						break;
					case "MCE":
						values.put("MCE", Integer.parseInt(terser.get("/.OBSERVATION(" + i + ")/.OBX-5")));
						break;
					case "ACF":
						values.put("ACF", Integer.parseInt(terser.get("/.OBSERVATION(" + i + ")/.OBX-5")));
						break;
					case "G":
						values.put("G", Integer.parseInt(terser.get("/.OBSERVATION(" + i + ")/.OBX-5")));
						break;

					case "TPI":
						values.put("TPI", Integer.parseInt(terser.get("/.OBSERVATION(" + i + ")/.OBX-5")));
						break;
					case "MAXV":
						values.put("MAXV", Integer.parseInt(terser.get("/.OBSERVATION(" + i + ")/.OBX-5")));
						break;
					case "MAXV-t":
						values.put("MAXVt", Integer.parseInt(terser.get("/.OBSERVATION(" + i + ")/.OBX-5")));
						break;
					case "AUC":
						values.put("AUC", Integer.parseInt(terser.get("/.OBSERVATION(" + i + ")/.OBX-5")));
						break;
					case "LT":
						values.put("LT", Integer.parseInt(terser.get("/.OBSERVATION(" + i + ")/.OBX-5")));
						break;

					default:
					}

				}

			}
			String txt = "Platzhalter";
			if (dbHandler.saveMsg(patID, msgType, values, txt, imgName))
				this.moveFile(filePath, fileName);			

			dbHandler.close();

		} catch (HL7Exception e) {
			e.printStackTrace();
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}

	}













}
