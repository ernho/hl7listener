package hl7listener;

import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.Collections;
import java.util.List;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class Main {
	
	
	static Parser parser;
	static String path ;
	
	
	Main(){
		
		  parser = new Parser();
	
	}
	
	/**
	 * 
	 * Waits for incoming messages and parse them. 
	 * 
	 */
	void watchDir(String url){

		Path myDir = Paths.get(url);
        System.out.println("INFO: Starting listing for new message in folder "+path);

        try {      	           
           FileSystem fileSystem = FileSystems.getDefault();
           WatchService watcher = fileSystem.newWatchService();  
           myDir.register(watcher, StandardWatchEventKinds.ENTRY_CREATE, 
           StandardWatchEventKinds.ENTRY_DELETE, StandardWatchEventKinds.ENTRY_MODIFY);
           
           boolean valid = true;
           do{
        	   WatchKey watckKey = watcher.take();
               List<WatchEvent<?>> events = watckKey.pollEvents();
               for (WatchEvent<?> event : events) {
                    if (event.kind() == StandardWatchEventKinds.ENTRY_CREATE) {
                    	String fileName = event.context().toString();
                        String extension = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
                        
                    	Thread.sleep(5000L);
                        if(extension.equals("hl7"))
                        {
                        	parser.parseMsg(path,fileName);
                        }
                        else if(extension.equals("jpg"))
                        {	
                        	System.out.println("Image found with name "+fileName);
                        	parser.parseImg(path,fileName);
                        	
                        }
                        	
                        
                    }
                
               }
        	   
   			valid = watckKey.reset();
           }while(valid);
         
            
           
        } catch (Exception e) {
            System.out.println("Error: " + e.toString());
        }
	}

    public static void main(String[] args) {
    	@SuppressWarnings("unchecked")
		List<Logger> loggers = Collections.<Logger>list(LogManager.getCurrentLoggers());
    	loggers.add(LogManager.getRootLogger());
    	for ( Logger logger : loggers ) {
    	    logger.setLevel(Level.OFF);
    	}
    	
    	// TODO load this information from properties file or database.
    	Main prog = new Main();
    	if(args.length!=5){
    		System.out.println("ERROR Ussage: folder_to_watch folder_to_image/ dbuser dbname");
    		System.exit(-1);
    	}else{
    		path = args[0];
    		parser.setPathToCopyMsg(args[1]);
    		parser.setPathToCopyImg(args[2]);
			DBHandler.userName = args[3];
			DBHandler.password = args[4];
			
			

    		
    	}
    	prog.watchDir(path);

    
	
			
	}
}